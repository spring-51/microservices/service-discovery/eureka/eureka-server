# Service discovery by Eureka

```
Eureka is one of the service discovery tool, that helps its client to call 
registered service without memorising instance ip , port etc.
```

### Steps to config Eureka server

```
s1 - Create and start eureka server
  1.1 - create spring-boot app from start.spring.io
  1.2 - add below dependency
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
        </dependency>
  1.3 - Update application.yml with following properties
  server:
    port: 8761
  eureka:
    client:
      register-with-eureka: false
      fetch-registry: false
      
  1.4 - Add @EnableEurekaServer on Spring boot config class
  1.5 - Strat Eureca server
  1.6 - goto http://127.0.0.1:8761/ 
      - it this site load eureca server details then our eureca server is config
        and ready for usage
         

s2 - Config Eureca server discovery in client app
  2.1 - add below dependency
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
  2.2 - add @EnableEurekaClient on config class of client app
  2.3 - give the app unique name in application.yml
  spring:
    application:
      name: DEPARTMENT-SERVICE
  
  2.4 - add Running Eureca server (as mention in s1) config in application.yml
  eureka:
    client:
      enabled: true
      fetch-registry: true
      register-with-eureka: true
      service-url:
        defaultZone: http://127.0.0.1:8761/eureka
    instance:
      hostname: localhost
  
  2.5 - Restart the 'n' instabce of this client app
  2.6 - Once Client app restarted , we will see it in http://127.0.0.1:8761/ portal  
  2.7 - refer - https://gitlab.com/spring-51/microservices/service-discovery/eureka/eureka-clients
  
Note:
 1. S1 is optional, if we already have some eureka server running.
 2. in S2, once the client app is up, we can call it by app name from other registered services as
    http://<app-name-of-service2>/...
    eg. http://DEPARTMENT-SERVICE/...
    refer - https://gitlab.com/spring-51/microservices/service-discovery/eureka/eureka-clients/user-service/-/blob/master/src/main/resources/application.yml
```


### Without Eureka (or any Service discovery)
```
PROBLEMS

1. We need to define/ create our own load balancing.
2. while calling any service we need to know ip, port etc
  e.g. 
  to call service2 from service1 we need to difine end point of service2 in service1 as
  http://<ip-of-service2>:<port-of-service2>/...  

  refer - https://gitlab.com/spring-51/microservices/service-discovery/eureka/eureka-clients/user-service/-/blob/master/src/main/resources/application.yml

```

### With Eureka (or any Service discovery)
```
PROBLEMS SOLVED

1. Service discovery difine their own load balancing strategy.
2. while calling any service we DO NOT  need to know ip, port etc
  e.g. 
  to call service2 from service1 we need to define end point of service2 in service1 as
  http://<app-name-of-service2>/...  
  
  refer - https://gitlab.com/spring-51/microservices/service-discovery/eureka/eureka-clients/user-service/-/blob/master/src/main/resources/application.yml

```
